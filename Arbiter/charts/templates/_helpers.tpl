{{/* vim: set filetype=mustache: */}}
{{/*
Get Arbiter version from appVersion or image tag.
*/}}
{{- define "arbiter.version" -}}
{{- default .Chart.AppVersion .Values.image.tag -}}
{{- end -}}
