{{/* vim: set filetype=mustache: */}}
{{/*
Get cloud_gateway version from appVersion or image tag.
*/}}
{{- define "cloudGateway.imageVersion" -}}
{{- default .Chart.AppVersion .Values.imageVersion -}}
{{- end -}}
