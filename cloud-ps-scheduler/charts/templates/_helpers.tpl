{{/* vim: set filetype=mustache: */}}

{{/*
Make sure app prefix contains a hyphen at the end
*/}}
{{- define "cloud-ps-scheduler.prefix" -}}
{{- if .Values.appPrefix -}}
    {{- $name := .Values.appPrefix | trimSuffix "-" -}}
    {{- printf "%s-" $name -}}
{{- else }}
    {{- print "" -}}
{{- end -}}
{{- end -}}

{{/*
Prepend the app prefix to the Chart name if it is given
*/}}
{{- define "cloud-ps-scheduler.name" -}}
{{- printf "%s%s" (include "cloud-ps-scheduler.prefix" . ) .Chart.Name -}}
{{- end -}}


{{/*
Defaults to the AppVersion unless a image tag is given
*/}}
{{- define "cloud-ps-scheduler.version" -}}
{{- default .Chart.AppVersion .Values.image.tag -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "cloud-ps-scheduler.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "cloud-ps-scheduler.labels" -}}
helm.sh/chart: {{ include "cloud-ps-scheduler.chart" . }}
{{ include "cloud-ps-scheduler.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ include "cloud-ps-scheduler.version" . | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "cloud-ps-scheduler.selectorLabels" -}}
k8s-app: {{ include "cloud-ps-scheduler.name" . }}
{{- end -}}
