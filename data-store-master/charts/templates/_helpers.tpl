{{/* vim: set filetype=mustache: */}}
{{/*
Get KM version from appVersion or image tag.
*/}}
{{- define "dataStoreMaster.version" -}}
{{- default .Chart.AppVersion .Values.image.tag -}}
{{- end -}}
