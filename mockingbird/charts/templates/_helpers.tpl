{{/* vim: set filetype=mustache: */}}
{{/*
Make sure app prefix contains a hyphen at the end
*/}}
{{- define "mockingbird.prefix" -}}
{{- if .Values.appPrefix -}}
    {{- $name := .Values.appPrefix | trimSuffix "-" -}}
    {{- printf "%s-" $name -}}
{{- else -}}
    {{- print "" -}}
{{- end -}}
{{- end -}}

{{/*
Prepend the app prefix to the Chart name if it is given 
*/}}
{{- define "mockingbird.name" -}}
{{- printf "%s%s" (include "mockingbird.prefix" . ) .Chart.Name -}}
{{- end -}}

{{/*
Default to the AppVersion unless an image tag is given
*/}}
{{- define "mockingbird.version" -}}
{{- default .Chart.AppVersion .Values.image.tag -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "mockingbird.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "mockingbird.labels" -}}
helm.sh/chart: {{ include "mockingbird.chart" . }}
{{ include "mockingbird.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ include "mockingbird.version" . | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "mockingbird.selectorLabels" -}}
k8s-app: {{ include "mockingbird.name" . }}
{{- end -}}